EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2300 3100 2300 2950
Connection ~ 2250 2900
Wire Wire Line
	2250 2800 2250 2900
Connection ~ 2250 3000
Wire Wire Line
	2250 3000 2350 3000
Wire Wire Line
	2250 2900 2250 3000
Wire Wire Line
	2250 3100 2300 3100
Wire Wire Line
	2350 3200 2250 3200
Wire Wire Line
	2250 3300 2550 3300
Connection ~ 2250 3500
Wire Wire Line
	2250 3500 2400 3500
Wire Wire Line
	2250 3500 2250 3400
$Comp
L CO2Mofetten:RJ45SensorCableConnector J1
U 1 1 5DDDF88C
P 1850 3200
F 0 "J1" H 1907 3867 50  0000 C CNN
F 1 "RJ45SensorCableConnector" H 1907 3776 50  0000 C CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 1850 3225 50  0001 C CNN
F 3 "~" V 1850 3225 50  0001 C CNN
	1    1850 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E30F682
P 2650 6750
F 0 "#PWR0101" H 2650 6500 50  0001 C CNN
F 1 "GND" H 2655 6577 50  0000 C CNN
F 2 "" H 2650 6750 50  0001 C CNN
F 3 "" H 2650 6750 50  0001 C CNN
	1    2650 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 5550 2850 5550
Wire Wire Line
	2650 5550 2650 6600
Connection ~ 2950 5550
Wire Wire Line
	2950 6000 2950 6100
$Comp
L power:+5V #PWR0102
U 1 1 5E317D90
P 1050 800
F 0 "#PWR0102" H 1050 650 50  0001 C CNN
F 1 "+5V" H 1065 973 50  0000 C CNN
F 2 "" H 1050 800 50  0001 C CNN
F 3 "" H 1050 800 50  0001 C CNN
	1    1050 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3500 2400 6100
Connection ~ 2350 3100
Wire Wire Line
	2350 3100 2350 3200
Wire Wire Line
	2350 3000 2350 3100
Wire Wire Line
	2800 3100 2800 5300
Wire Wire Line
	2350 3100 2500 3100
Wire Wire Line
	2550 2700 2700 2700
Wire Wire Line
	2550 2700 2550 3300
Wire Wire Line
	2300 2950 2800 2950
Wire Wire Line
	2800 2950 2800 2800
$Comp
L CO2Mofetten:Adafruit_DS3231 U1
U 1 1 5E2AF338
P 1850 1800
F 0 "U1" H 1817 1135 50  0000 C CNN
F 1 "Adafruit_DS3231" H 1817 1226 50  0000 C CNN
F 2 "CO2Mofetten:Adafruit_DS3231_PinSocket" H 1850 2250 50  0001 C CNN
F 3 "https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/overview" H 1817 1227 50  0001 C CNN
	1    1850 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	2200 2100 2400 2100
Wire Wire Line
	2400 2100 2400 3500
Connection ~ 2400 3500
Wire Wire Line
	2200 2000 2500 2000
Wire Wire Line
	2500 2000 2500 3100
Connection ~ 2500 3100
Wire Wire Line
	2500 3100 2800 3100
Wire Wire Line
	2200 1900 2800 1900
Wire Wire Line
	2800 1900 2800 2800
Connection ~ 2800 2800
Wire Wire Line
	2200 1800 2700 1800
Wire Wire Line
	2700 1800 2700 2700
Connection ~ 2700 2700
Wire Wire Line
	5550 5900 5850 5900
Wire Wire Line
	5850 5900 5850 2750
Wire Wire Line
	5550 6000 5700 6000
Wire Wire Line
	5700 6000 5700 2650
Wire Wire Line
	4200 1450 4200 1250
Wire Wire Line
	4200 1250 5800 1250
Wire Wire Line
	5800 1250 5800 2850
Wire Wire Line
	5800 6100 5550 6100
$Comp
L CO2Mofetten:Adafruit_MicroSD U2
U 1 1 5E30AE8B
P 8900 3000
F 0 "U2" H 9278 3246 50  0000 L CNN
F 1 "Adafruit_MicroSD" H 9278 3155 50  0000 L CNN
F 2 "CO2Mofetten:Adafruit_MicroSD_PinSocket" H 8900 3700 50  0001 C CNN
F 3 "" H 8900 3700 50  0001 C CNN
	1    8900 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 6600 6200 2950
Wire Wire Line
	6200 2950 7600 2950
Connection ~ 2650 6600
Wire Wire Line
	2650 6600 2650 6750
Wire Wire Line
	8500 2850 5800 2850
Connection ~ 5800 2850
Wire Wire Line
	8500 2750 5850 2750
Connection ~ 5850 2750
Wire Wire Line
	5850 2750 5850 1150
Wire Wire Line
	8500 2650 5700 2650
Connection ~ 5700 2650
Wire Wire Line
	5700 2650 5700 1350
Wire Wire Line
	2650 6600 6200 6600
Wire Wire Line
	4300 1050 4300 1450
Wire Wire Line
	4300 1050 2600 1050
Wire Wire Line
	2600 1050 2600 6000
Connection ~ 2600 6000
Wire Wire Line
	1050 800  1050 850 
Wire Wire Line
	1050 6000 2600 6000
Wire Wire Line
	1050 850  6200 850 
Wire Wire Line
	6200 850  6200 1500
Connection ~ 1050 850 
Wire Wire Line
	1050 850  1050 6000
Connection ~ 6200 2450
Wire Wire Line
	6200 2450 8500 2450
$Comp
L Device:R R1
U 1 1 5E35197C
P 6200 1650
F 0 "R1" H 6270 1696 50  0000 L CNN
F 1 "10k" H 6270 1605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 6130 1650 50  0001 C CNN
F 3 "~" H 6200 1650 50  0001 C CNN
	1    6200 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 1800 6200 2450
Wire Wire Line
	4400 1450 4400 1400
Wire Wire Line
	2850 1400 2850 5550
Connection ~ 2850 5550
Wire Wire Line
	2850 5550 2650 5550
Connection ~ 2950 6000
Connection ~ 2950 6100
Connection ~ 2950 5300
Connection ~ 2950 5400
Wire Wire Line
	2950 5300 2950 5400
Connection ~ 2950 5500
Wire Wire Line
	2950 5400 2950 5500
Wire Wire Line
	2950 5550 2950 5500
Connection ~ 2950 5600
Wire Wire Line
	2950 5600 2950 5550
Wire Wire Line
	2950 5700 2950 5600
Wire Wire Line
	2600 6000 2950 6000
Wire Wire Line
	2800 5300 2950 5300
Wire Wire Line
	2800 2800 2950 2800
Wire Wire Line
	2700 2700 2950 2700
Wire Wire Line
	4400 1400 2850 1400
$Comp
L Alarm-Siren_Arduino:Arduino_Mega2560_Shield XA1
U 1 1 5E29DFAA
P 4250 4050
F 0 "XA1" H 4250 1669 60  0000 C CNN
F 1 "Arduino_Mega2560_Shield" H 4250 1563 60  0000 C CNN
F 2 "Alarm-Siren Arduino:Arduino_Mega2560_Shield" H 4950 6800 60  0001 C CNN
F 3 "https://store.arduino.cc/arduino-mega-2560-rev3" H 4950 6800 60  0001 C CNN
	1    4250 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1450 4100 1350
Wire Wire Line
	4100 1350 5700 1350
Wire Wire Line
	4000 1150 4000 1450
Wire Wire Line
	4000 1150 5850 1150
Wire Wire Line
	5900 6700 2850 6700
Wire Wire Line
	2850 6700 2850 5900
Wire Wire Line
	2850 5900 2950 5900
Wire Wire Line
	5650 2450 6200 2450
Wire Wire Line
	5600 2550 8500 2550
Wire Wire Line
	5650 2450 5650 2200
Wire Wire Line
	5650 2200 5550 2200
Wire Wire Line
	5600 2550 5600 2100
Wire Wire Line
	5600 2100 5550 2100
Wire Wire Line
	5900 2250 5900 6700
Wire Wire Line
	2900 3200 2950 3200
Wire Wire Line
	2900 3200 2900 1100
Wire Wire Line
	2900 1100 6050 1100
Wire Wire Line
	6050 1100 6050 2150
Wire Wire Line
	5800 2850 5800 6100
Wire Wire Line
	6050 2150 7100 2150
Wire Wire Line
	8000 3150 8500 3150
$Comp
L dk_Transistors-FETs-MOSFETs-Single:IRLML6401TRPBF Q1
U 1 1 5E37A0F2
P 7400 2050
F 0 "Q1" H 7508 2103 60  0000 L CNN
F 1 "IRLML6401TRPBF" H 7508 1997 60  0000 L CNN
F 2 "digikey-footprints:SOT-23-3" H 7600 2250 60  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irlml6401pbf.pdf?fileId=5546d462533600a401535668b96d2634" H 7600 2350 60  0001 L CNN
F 4 "IRLML6401PBFCT-ND" H 7600 2450 60  0001 L CNN "Digi-Key_PN"
F 5 "IRLML6401TRPBF" H 7600 2550 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 7600 2650 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 7600 2750 60  0001 L CNN "Family"
F 8 "https://www.infineon.com/dgdl/irlml6401pbf.pdf?fileId=5546d462533600a401535668b96d2634" H 7600 2850 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/infineon-technologies/IRLML6401TRPBF/IRLML6401PBFCT-ND/812509" H 7600 2950 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET P-CH 12V 4.3A SOT-23" H 7600 3050 60  0001 L CNN "Description"
F 11 "Infineon Technologies" H 7600 3150 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7600 3250 60  0001 L CNN "Status"
	1    7400 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2250 6500 2250
Wire Wire Line
	8000 1850 7400 1850
$Comp
L dk_RF-Transceiver-Modules:XB24CAWIT-001 MOD1
U 1 1 5E3748E0
P 8400 4950
F 0 "MOD1" H 8400 5753 60  0000 C CNN
F 1 "XB24CAWIT-001" H 8400 5647 60  0000 C CNN
F 2 "digikey-footprints:XBEE-20_THT" H 8600 5150 60  0001 L CNN
F 3 "https://www.digi.com/resources/documentation/digidocs/pdfs/90001500.pdf" H 8600 5250 60  0001 L CNN
F 4 "602-1891-ND" H 8600 5350 60  0001 L CNN "Digi-Key_PN"
F 5 "XB24CAWIT-001" H 8600 5450 60  0001 L CNN "MPN"
F 6 "RF/IF and RFID" H 8600 5550 60  0001 L CNN "Category"
F 7 "RF Transceiver Modules" H 8600 5650 60  0001 L CNN "Family"
F 8 "https://www.digi.com/resources/documentation/digidocs/pdfs/90001500.pdf" H 8600 5750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/digi-international/XB24CAWIT-001/602-1891-ND/6010100" H 8600 5850 60  0001 L CNN "DK_Detail_Page"
F 10 "RF TXRX MOD 802.15.4  WIRE ANT" H 8600 5950 60  0001 L CNN "Description"
F 11 "Digi International" H 8600 6050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8600 6150 60  0001 L CNN "Status"
	1    8400 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 5550 6450 5550
Wire Wire Line
	6450 5550 6450 6750
Wire Wire Line
	6450 6750 2650 6750
Connection ~ 2650 6750
Wire Wire Line
	10600 3750 10600 800 
Wire Wire Line
	10600 800  2750 800 
Wire Wire Line
	2750 800  2750 2100
Wire Wire Line
	2750 2100 2950 2100
Wire Wire Line
	10350 3850 10350 700 
Wire Wire Line
	10350 700  2650 700 
Wire Wire Line
	2650 700  2650 2200
Wire Wire Line
	2650 2200 2950 2200
Wire Wire Line
	2400 6100 2950 6100
Wire Wire Line
	8400 4350 8400 3050
Wire Wire Line
	8400 3050 8500 3050
$Comp
L CO2Mofetten:LogicLevelTranslator_4Channel U3
U 1 1 5E3E38D1
P 9650 3700
F 0 "U3" H 9650 3227 50  0000 C CNN
F 1 "LogicLevelTranslator_4Channel" H 9650 3136 50  0000 C CNN
F 2 "CO2Mofetten:LevelTranslator_4Channel_KeeYees_Breakout" H 9650 4400 50  0001 C CNN
F 3 "" H 9650 4400 50  0001 C CNN
F 4 "https://www.amazon.de/gp/product/B07LG6RK7L/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1" H 9650 3700 50  0001 C CNN "Amazon"
	1    9650 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 3050 8400 3050
Connection ~ 8400 3050
Connection ~ 6500 2250
Wire Wire Line
	6500 2250 7400 2250
Wire Wire Line
	7900 3050 7900 3300
Wire Wire Line
	7900 3300 9700 3300
Wire Wire Line
	9700 3300 9700 3400
Wire Wire Line
	6500 3350 9800 3350
Wire Wire Line
	9800 3350 9800 3400
Wire Wire Line
	6500 2250 6500 3350
Wire Wire Line
	9500 3400 9600 3400
Wire Wire Line
	9500 3400 7600 3400
Wire Wire Line
	7600 3400 7600 2950
Connection ~ 9500 3400
Connection ~ 7600 2950
Wire Wire Line
	7600 2950 8500 2950
Wire Wire Line
	9100 4550 9150 4550
Wire Wire Line
	9150 4550 9150 3750
Wire Wire Line
	9150 3750 9300 3750
Wire Wire Line
	9300 3850 7500 3850
Wire Wire Line
	7500 3850 7500 4550
Wire Wire Line
	7500 4550 7700 4550
Wire Wire Line
	10600 3750 10000 3750
Wire Wire Line
	10350 3850 10000 3850
Wire Wire Line
	2900 6450 2900 5000
Wire Wire Line
	2450 5000 2450 950 
Wire Wire Line
	2450 950  4500 950 
Wire Wire Line
	4500 950  4500 1450
Wire Wire Line
	2450 5000 2900 5000
Connection ~ 2900 5000
Wire Wire Line
	2900 5000 2950 5000
Wire Wire Line
	9100 4850 9250 4850
Wire Wire Line
	9250 4850 9250 3950
Wire Wire Line
	9250 3950 9300 3950
Wire Wire Line
	10000 3950 10950 3950
Wire Wire Line
	10950 3950 10950 6450
Wire Wire Line
	2900 6450 10950 6450
Wire Wire Line
	8000 1850 8000 3150
$EndSCHEMATC
